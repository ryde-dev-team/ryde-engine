//This file is the client "template" file so to speak. It does not need to be included but it will be heavily referenced by your client.cpp

class EventReceiver : public IEventReceiver{
	public:
		//This function overrides the IEventReceiver so that we can use it instead. This is called for every Irrlict Event. This could of corse be over ridden by your own class
		virtual bool OnEvent(const SEvent& event){
			//Update our key state array with the pressed key
			if (event.EventType == EET_KEY_INPUT_EVENT)
				keyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;

			//Check all keys in the key database and update the related arrays accordingly
			for (u32 i = 0; i < KEY_KEY_CODES_COUNT; ++i){
				//If the key is actually down right now
				if (keyIsDown[i] == true){
					//And the key has not been pressed before
					if (keyIsReleased[i] == true){
						//Say the the key has been pressed but has not been checked
						keyIsPressed[i] = true;

						//And the key has not been released yet
						keyIsReleased[i] = false;
					}
				}
				//If the key is not currently down
				else {
					//Key is not pressed
					keyIsPressed[i] = false;

					//And key has been released
					keyIsReleased[i] = true;
				}
			}

			//If the event originated from the GUI
			if (event.EventType == EET_GUI_EVENT){
				//Update our GUI Event array with the event type
				guiElementEvent[event.GUIEvent.Caller->getID()] = event.GUIEvent.EventType;
			}


			return false;
		}

		// This is used to check whether a key is being held down
		virtual bool isKeyDown(EKEY_CODE keyCode) const
		{
			return keyIsDown[keyCode];
		}

		

		//Used to check if the gui element had an event and return it if it did
		virtual int getGUIEvent(u32 rfGui)
		{
			int rtVal = guiElementEvent[rfGui];
			guiElementEvent[rfGui] = -1;
			return rtVal;
		}

		//Used to reset the event of the gui element back to default
		int resetGui(u32 rfGui){
			guiElementEvent[rfGui] = -1;
			return 0;
		}

		//Checks if the key has been pressed
		//If it has, release the event and don't check the key until it has been released and pressed again
		virtual bool isKeyPressed(EKEY_CODE keyCode){
			bool rtn = keyIsPressed[keyCode];
			keyIsPressed[keyCode] = false;
			return rtn;
		}

		//Set all the default values in order to avoid nulls
		EventReceiver()
		{
			for (u32 i = 0; i<KEY_KEY_CODES_COUNT; ++i)
				keyIsDown[i] = false;
			for (u32 i = 0; i<KEY_KEY_CODES_COUNT; ++i)
				keyIsPressed[i] = false;
			for (u32 i = 0; i<KEY_KEY_CODES_COUNT; ++i)
				keyIsReleased[i] = true;
			for (u32 i = 0; i < 99; ++i)
				guiElementEvent[i] = -1;
		}
	private:
		bool keyIsDown[KEY_KEY_CODES_COUNT];
		bool keyIsPressed[KEY_KEY_CODES_COUNT];
		bool keyIsReleased[KEY_KEY_CODES_COUNT];

		int guiElementEvent[999];
};

class Client{
		public:
			//Network variables
			RakPeerInterface* peer;
			Packet* packet;
			RakNetGUID guid;
			SystemAddress serverSA;
			const char* serverAddress;
			unsigned int serverPort;

			//Graphics variables
			IrrlichtDevice* device;
			IVideoDriver* driver;
			ISceneManager* scene;
			IGUIEnvironment* gui;
			EventReceiver events;
			list<User*> userList;

			//Custom variables
			int width;
			int height;

			RakString appTitle;
			RakString versionInfo;

			RakString username;
			RakString password;

			//Just by starting a new client you start the graphics and network system.
			Client(RakString tmpTitle = "Ryde Client", RakString tmpVersionInfo = "v1.0", int tmpWidth = 800, int tmpHeight = 600, const char* tmpSAddr = "127.0.0.1", unsigned int tmpPort = 60000){
				serverAddress = tmpSAddr;
				serverPort = tmpPort;
				appTitle = tmpTitle;
				width = tmpWidth;
				height = tmpHeight;
				versionInfo = tmpVersionInfo;

				startNetwork();
				startGraphics();
			}

			~Client(){
				//Kill graphics and network safely
				RakPeerInterface::DestroyInstance(peer);
				device->drop();
			}

			//This is the init for the network, it is called when the client is created.
			int startNetwork(void){
				SocketDescriptor testSD;

				peer = RakPeerInterface::GetInstance();

				//Null catch
				if (!peer)
					return 1;

				//Start raknet with only 1 connection(Server) our socket and with only 1 socket
				peer->Startup(1,&testSD, 1);

				guid = peer->GetGuidFromSystemAddress(UNASSIGNED_SYSTEM_ADDRESS);
				return 0;
			}

			//This is called in the clients load network function
			int connectToServer(){
				peer->Connect(serverAddress, serverPort, 0, 0);
				return 0;
			}

			//This function is used to get the next packet in cue
			int nextPacket(){
				if (packet){
					peer->DeallocatePacket(packet);
					packet = peer->Receive();
				}
				else {
					packet = peer->Receive();
				}
				return 0;
			}

			MessageID getPacketHeader(){
				if (packet == NULL) return 255;
				if (packet->data[0] < 1) return 255;
				return packet->data[0];
			}

			//Called when the server accepts our handshake request
			int confirmHandshake(){
				serverSA = packet->systemAddress;
				return 0;
			}

			//This command is sent to the server to request a login. It will send back RN_LOGIN_ACCEPT or RN_LOGIN_INVALID
			int login(){
				BitStream bsOut;
				bsOut.Write((MessageID)RN_LOGIN_REQUEST);
				bsOut.Write(username);
				bsOut.Write(password);
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverSA, false);

				return 0;
			}

			//This is the init for the graphics, it is called when the client is created
			int startGraphics(){
				//Start the irrlicht device with the Software graphics driver type DirectX or OpenGL, with the width and height 
				//of the client object, 32 color depth, not fullscreen, not with stencil buffer, with vSync, and with our custom 
				//event reciever.
				device = createDevice(EDT_SOFTWARE, dimension2d<u32>(width, height), 32, false, false, true, &events);

				//Null catch
				if (!device) return 1;

				//Set all of the object params
				device->setWindowCaption(appTitle.ToWideChar());
				driver = device->getVideoDriver();
				scene = device->getSceneManager();
				gui = device->getGUIEnvironment();

				return 0;
			}

			int updateGraphics(){
				driver->beginScene(true, true, SColor(255, 100, 101, 140));

				scene->drawAll();
				gui->drawAll();

				driver->endScene();
				return 0;
			}

			

			int sendHandshake(){
				BitStream bsOut;
				bsOut.Write((MessageID)RN_HANDSHAKE_REQUEST);
				bsOut.Write(guid.ToString());
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				return 0;
			}


			int sendCommand(RydeMessage tmpCmd, RakString tmpData){
				BitStream bsOut;
				bsOut.Write((MessageID)tmpCmd);
				bsOut.Write(tmpData);
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverSA, false);
				return 0;
			}

			int readCommand(Packet* tmpPacket, RakString tmpStr){
				NetMessage tmpMsg;
				tmpMsg.nextMsg(tmpPacket);
				tmpStr = tmpMsg.readRS();
				return 0;
			}

			

			int sendPos(vector3df tmpNode){
				//Works
				BitStream bsOut;
				bsOut.Write((MessageID)RN_UPDATE_POSITION);
				bsOut.Write((int)tmpNode.X);
				bsOut.Write((int)tmpNode.Y);
				bsOut.Write((int)tmpNode.Z);


				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, serverSA, false);

				

				

				return 0;
			}

			User* getUser(RakString tmpUN){

				for (User* tmpUser : userList){
					if (tmpUser->un == tmpUN){
						return tmpUser;
					}
				}
				User* rtUser = new User;
				userList.push_front(rtUser);
				return rtUser;
			}

		private:
	};
