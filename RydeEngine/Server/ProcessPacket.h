//Server
int processPacket(){
	switch (thisServer->getPacketHeader())
	{
		//RakNet Messages
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			printf("Our connection request has been accepted.\n");
			break;
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("A client has disconnected.\n");
			break;
		case ID_CONNECTION_LOST:
			printf("A client lost the connection.\n");
			break;


		//RydeNetwork Messages

		//Client sends this when it wants an official respose saying the session has been init
		case RN_HANDSHAKE_REQUEST:
		{
			thisServer->newSession();

			User* tmpUser = thisServer->getUser(thisServer->packet->systemAddress);

			printf("Client sent handshake request with GUID %s\n", tmpUser->guid.C_String());

			printf("Replying with handshake accept\n");
			thisServer->sendCommand(RN_HANDSHAKE_ACCEPT, "");
		}break;

		//This is sent when the client has entered a username and password and pressed enter
		case RN_LOGIN_REQUEST:
		{
			RakString recUN;
			RakString recPW;
			BitStream* bsIn = new BitStream(thisServer->packet->data, thisServer->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(recUN);
			bsIn->Read(recPW);
			if (thisServer->login(thisServer->packet->systemAddress, recUN, recPW)){
				printf("%s: Logging in with username %s\n", thisServer->packet->systemAddress.ToString(false), recUN.C_String());
				thisServer->sendCommand(RN_LOGIN_ACCEPT, "");
			}
			else
			{
				printf("%s: Invalid login U:%s P:%s\n", thisServer->packet->systemAddress.ToString(false), recUN.C_String(), recPW.C_String());
				thisServer->sendCommand(RN_LOGIN_INVALID, "");
			}

		}break;

		//This is received when a user sends a chat message
		case RN_CHAT_MESSAGE:
		{
			User* tmpUser = thisServer->getUser(thisServer->packet->systemAddress);

			RakString msgStr;

			BitStream* bsIn = new BitStream(thisServer->packet->data, thisServer->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(msgStr);

			RakString tmpUN = tmpUser->un.C_String();
			RakString chatStr = tmpUN + ":" + msgStr.C_String();

			printf("%s\n", chatStr.C_String());

			BitStream bsOut;
			bsOut.Write((MessageID)RN_CHAT_MESSAGE);
			bsOut.Write(chatStr);
			thisServer->peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_SYSTEM_ADDRESS, true);

		}break;

		//Sent every time a client needs to update their position to the relevant clients around them
		case RN_UPDATE_POSITION:{
			//Works
			vector3df userPos;

			const char* x;
			const char* y;
			const char* z;

			BitStream* bsIn = new BitStream(thisServer->packet->data, thisServer->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(x);
			bsIn->Read(y);
			bsIn->Read(z);

			userPos.X = (int)x;
			userPos.Y = (int)y;
			userPos.Z = (int)z;

			User* tmpUser = thisServer->getUser(thisServer->packet->systemAddress);
			tmpUser->pos = userPos;

			BitStream bsOut;
			bsOut.Write((MessageID)RN_UPDATE_POSITION);
			bsOut.Write(tmpUser->un);
			bsOut.Write((int)userPos.X);
			bsOut.Write((int)userPos.Y);
			bsOut.Write((int)userPos.Z);
			thisServer->peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_SYSTEM_ADDRESS, true);
		}break;

		//Null Catch
		case 255:{
			break;
		}
		default:
			printf("Message with identifier %i has arrived.\n", thisServer->getPacketHeader());
			break;
		}
	return 0;
}