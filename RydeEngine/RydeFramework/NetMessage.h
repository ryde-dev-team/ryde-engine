
	class NetMessage{
		
		public:

			NetMessage(){
				//BitStream* bsOut = new BitStream();
			}

			int add(const wchar_t* tmpMsg){
				bsOut.Write((const wchar_t*)tmpMsg);
				return 0;
			}

			int add(const char* tmpMsg){
				bsOut.Write((const char*)tmpMsg);
				return 0;
			}

			int add(MessageID tmpMsg){
				bsOut.Write((MessageID) tmpMsg);
				return 0;
			}

			int add(RakString tmpMsg){
				bsOut.Write((RakString) tmpMsg);
				return 0;
			}

			int nextMsg(Packet* tmpPacket){
				bsIn = new BitStream(tmpPacket->data, tmpPacket->length, false);
				bsIn->IgnoreBytes(sizeof(MessageID));
				return 0;
			}

			int read(const char* tmpMsg){
				bsIn->Read((const char*)tmpMsg);
				return 0;
			}

			RakString readRS(){
				RakString rs = "[NULL]";
				bsIn->Read((RakString)rs);
				return rs;
			}

			const char* readC(){
				const char* tmpStr = "[NULL]";
				bsIn->Read(tmpStr);
				return tmpStr;
			}

			

			int send(RakPeerInterface* tmpIntFace, SystemAddress tmpSA){
				tmpIntFace->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, tmpSA, false);
				return 0;
			}

			int sendAll(RakPeerInterface* tmpIntFace, Packet* tmpPacket){
				tmpIntFace->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_SYSTEM_ADDRESS, true);
				return 0;
			}

			
	private:
		BitStream* bsIn;
		BitStream bsOut;

	};
