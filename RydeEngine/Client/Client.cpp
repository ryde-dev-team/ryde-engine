#include <RydeFramework.h>

Client* thisClient = new Client;

enum RF_GUI{
	RF_GUI_UNBOX = 0,
	RF_GUI_PWBOX,
	RF_GUI_CHATWIN,
	RF_GUI_CHATBOX,
	RF_GUI_COUNT
};



//GUI Screens
#include "LoginScreen.h"
#include "GameGUI.h"

#include "ProcessPacket.h"

//Functions
#include "LoadingFunctions.h"
#include "MiscFunctions.h"

//Game Scenes
#include "GameScene.h"

int main(void){

	boot();
	login();
	loadGameGUI();
	loadGameScene();
	while (thisClient->device->run()){
		updateGameGUI();
		updateGameScene();

		updateNetwork();
		thisClient->updateGraphics();
	}
	delete thisClient;

	return 0;
}