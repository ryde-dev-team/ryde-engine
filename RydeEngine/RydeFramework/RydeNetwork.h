#include "RakPeerInterface.h"
#include "MessageIdentifiers.h"
#include "BitStream.h"
#include "RakNetTypes.h"
#include "RakSleep.h"

enum RydeMessage{
	RN_HANDSHAKE_REQUEST = ID_USER_PACKET_ENUM + 1,
	RN_HANDSHAKE_ACCEPT,
	RN_CHAT_MESSAGE,
	RN_LOGIN_REQUEST,
	RN_LOGIN_ACCEPT,
	RN_LOGIN_INVALID,
	RN_UPDATE_POSITION,
	RN_NEW_USER_LOGIN
};

using namespace RakNet;
