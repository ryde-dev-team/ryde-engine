int loadGraphics(){
	//Need to add all graphics loading here. Textures and meshes, load them all into memory here.
	return 0;
}

int loadNetwork(){
	printf("Connecting to server at %s\n", thisClient->serverAddress);
	thisClient->connectToServer();
	printf("Client GUID: %s\n", thisClient->guid.ToString());

	bool didHS = false;


	while (!didHS){
		thisClient->nextPacket();
		processPacket();
		if (thisClient->getPacketHeader() == RN_HANDSHAKE_ACCEPT){
			didHS = true;
		}
	}

	return 0;
}

int boot(){
	printf("Booting %s [%s]\n", thisClient->appTitle.C_String(), thisClient->versionInfo.C_String());

	//load the actual client network and graphics
	loadNetwork();
	loadGraphics();

	printf("Starting %s\n", thisClient->appTitle.C_String());
	return 0;
}
