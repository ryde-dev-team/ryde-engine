

int updateNetwork(){
	thisClient->nextPacket();
	while (thisClient->getPacketHeader() != 255){
		if (processPacket() == 1) return 1;
		thisClient->nextPacket();
	}
	return 0;
}

int login(){
	loadLoginScreen();
	
	while (thisClient->device->run()){
		updateLoginScreen();
		

		if (updateNetwork() == 1){
			break;
		}
		thisClient->updateGraphics();
	}

	
	
	return 0;
}

