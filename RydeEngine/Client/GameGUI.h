bool textInput = false;
IGUIEditBox* chatBox;

IGUIListBox* chatWindow;

int loadGameGUI(){
	thisClient->gui->addStaticText(thisClient->username.ToWideChar(),
		rect<s32>(10, 10, 260, 22));

	chatWindow = thisClient->gui->addListBox(rect<s32>(vector2d<s32>(10, 400), vector2d<s32>(300, 565)), 0, RF_GUI_CHATWIN);

	chatBox = thisClient->gui->addEditBox(L"", rect<s32>(vector2d<s32>(10, 575), vector2d<s32>(300, 590)), true, 0, RF_GUI_CHATBOX);
	chatBox->setWordWrap(true);

	chatBox->setText(L"Press enter to type");
	return 0;
}

int updateGameGUI(){
	if (thisClient->events.isKeyPressed(KEY_RETURN)){
		if (textInput == false){
			textInput = true;
			thisClient->gui->setFocus(chatBox);
			chatBox->setText(L"");
		}
		else {
			stringc sendStr = chatBox->getText();
			textInput = false;
			thisClient->gui->removeFocus(chatBox);
			if (sendStr != L""){
				cout << "Typed: " << sendStr.c_str() << "\n";
				thisClient->sendCommand(RN_CHAT_MESSAGE, (RakString)sendStr.c_str());
			}
			chatBox->setText(L"Press enter to type");
		}
	}

	return 0;
}

int killGameGUI(){

	return 0;
}