//Client
int processPacket(){
	switch (thisClient->getPacketHeader())
	{
		//RakNet Messages
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			printf("Another client has disconnected.\n");
			break;
		case ID_REMOTE_CONNECTION_LOST:
			printf("Another client has lost the connection.\n");
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			printf("Another client has connected.\n");
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			printf("Our connection request has been accepted. Sending Handshake request.\n");
			thisClient->sendHandshake();
		}
		break;
		case ID_NEW_INCOMING_CONNECTION:
			printf("A connection is incoming.\n");
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			printf("The server is full.\n");
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			printf("We have been disconnected.\n");
			break;
		case ID_CONNECTION_LOST:
			printf("Connection lost.\n");
			break;


		//RydeNetwork Messages

		//Server sends this when it accepts our handshake request
		case RN_HANDSHAKE_ACCEPT:
		{
			printf("Handshake accepted by server\n");
			thisClient->confirmHandshake();
		}
		break;

		//Server sends this when it has recieve a chat message relevant to me
		case RN_CHAT_MESSAGE:{
			RakString msgStr;

			BitStream* bsIn = new BitStream(thisClient->packet->data, thisClient->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(msgStr);

			printf("%s\n", msgStr.C_String());

			chatWindow->addItem(msgStr.ToWideChar());
			chatWindow->setSelected(chatWindow->getItemCount());
			//newChatMsg(msgStr);
		}break;

			//Server sends this if it accepts our login attempt
		case RN_LOGIN_ACCEPT:{
			printf("Successfully Logged In.\n");
			killLoginScreen();
			
		}return 1;

		case RN_LOGIN_INVALID:{
			printf("Login was Invalid.\n");
			unInput = true;
			pwInput = false;
			thisClient->gui->setFocus(unBox);
			unBox->setText(L"");
			pwBox->setText(L"");
		}break;

		//Server sends this when it has recieved a position update relevant to me
		case RN_UPDATE_POSITION:{
			vector3df rVec;

			RakString rUN;
			const char* x;
			const char* y;
			const char* z;

			BitStream* bsIn = new BitStream(thisClient->packet->data, thisClient->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(rUN);
			bsIn->Read(x);
			bsIn->Read(y);
			bsIn->Read(z);

			rVec.X = (int)x;
			rVec.Y = (int)y;
			rVec.Z = (int)z;

			if (rUN != thisClient->username){

				User* tmpUser = thisClient->getUser(rUN);
				tmpUser->un = rUN;
				tmpUser->pos.X = (int)x;
				tmpUser->pos.Y = (int)y;
				tmpUser->pos.Z = (int)z;

			}
		}break;

			//The server sends this when it has recieved a login relevant to me
		case RN_NEW_USER_LOGIN:{
			RakString rUN;

			BitStream* bsIn = new BitStream(thisClient->packet->data, thisClient->packet->length, false);
			bsIn->IgnoreBytes(sizeof(MessageID));
			bsIn->Read(rUN);

			if (rUN != thisClient->username){

				User* tmpUser = thisClient->getUser(rUN);
				tmpUser->un = rUN;
			}

		}
		case 255:
			break;
		default:
			printf("Message with identifier %i has arrived.\n", thisClient->getPacketHeader());
			break;
		}
	return 0;
}