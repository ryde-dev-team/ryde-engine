#include <RydeFramework.h>

Server* thisServer = new Server;

#include "ProcessPacket.h"

int main(void){

	thisServer->serverDir = ExePath().c_str();

	printf("Server Started. Dir is %s \n", thisServer->serverDir.C_String());
	while (1){
		thisServer->nextPacket();
		processPacket();
	}

	return 0;
}