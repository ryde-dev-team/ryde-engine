bool unInput = false;
bool pwInput = false;
IGUIEditBox* unBox;
IGUIEditBox* pwBox;

IGUIStaticText* title;


int loadLoginScreen(){

	//Size for the edit boxes
	int boxWidth = 150;
	int boxHeight = 25;

	//Box dimensions
	int upperLeftX = (thisClient->width / 2) - (boxWidth / 2); //225
	int upperLeftY = (thisClient->height / 2) - (boxHeight / 2) + 30;//118.5
	int bottomRightX = (thisClient->width / 2) + (boxWidth / 2);//375
	int bottomRightY = (thisClient->height / 2) + (boxHeight / 2) + 30;//487.5

	//Title dimensions
	int tx = ((thisClient->width / 2) - 47);
	int ty = (((thisClient->height / 2) - 15) - 125);
	int tx2 = ((thisClient->width / 2) + 47);
	int ty2 = (((thisClient->height / 2) + 15) - 125);

	//Set the title to the game name
	title = thisClient->gui->addStaticText(thisClient->appTitle.ToWideChar(),
		rect<s32>(vector2d<s32>(tx, ty), vector2d<s32>(tx2, ty2)));


	unBox = thisClient->gui->addEditBox(L"", rect<s32>(vector2d<s32>(upperLeftX, upperLeftY - 30), vector2d<s32>(bottomRightX, bottomRightY - 30)), true, 0, RF_GUI_UNBOX);
	pwBox = thisClient->gui->addEditBox(L"", rect<s32>(vector2d<s32>(upperLeftX, upperLeftY + 30), vector2d<s32>(bottomRightX, bottomRightY + 30)), true, 0, RF_GUI_PWBOX);
	pwBox->setPasswordBox(true);


	//Login box animation loop
	s32 alphaValue = 255;
	timer animTimer;
	unsigned int animTime = 200;
	animTimer.start();
	while (animTimer.getTime() <= animTime){
		unBox->move(vector2d<s32>(0, -1));
		pwBox->move(vector2d<s32>(0, -1));

		setSkinTransparency(alphaValue, thisClient->gui->getSkin());

		int tim = animTimer.getTime();
		float mod = (float)animTimer.getTime() / (float)animTime;

		alphaValue = mod * 255;
		thisClient->updateGraphics();
	}
	unBox->setVisible(true);
	pwBox->setVisible(true);
	animTimer.stop();
	setSkinTransparency(255, thisClient->gui->getSkin());


	pwBox->setText(L"password");
	unBox->setText(L"username");



	return 0;
}

int updateLoginScreen(){
	//If you press the tab key it will switch to the correct box
	if (thisClient->events.isKeyPressed(KEY_TAB)){
		if (unInput == true){
			unInput = false;
			pwInput = true;
			thisClient->gui->setFocus(pwBox);
			pwBox->setText(L"");
		}
		else{
			unInput = true;
			pwInput = false;
			thisClient->gui->setFocus(unBox);
			unBox->setText(L"");
		}
	}

	//If you press the enter key it will get the text in the boxes and send a login request with that text
	if (thisClient->events.isKeyPressed(KEY_RETURN)){

		stringc typedUN = unBox->getText();
		stringc typedPW = pwBox->getText();

		if (typedUN != L"" && typedUN != L"username"){
			if (typedPW != L"" && typedPW != L"password"){
				printf("Attempting to login.\n");
				thisClient->username = typedUN.c_str();
				thisClient->password = typedPW.c_str();
				thisClient->login();
			}
		}

	}

	//If the mouse clicks the username box or password box
	if (thisClient->events.getGUIEvent(RF_GUI_PWBOX) == 19){
		unInput = false;
		pwInput = true;
		pwBox->setText(L"");
	}

	if (thisClient->events.getGUIEvent(RF_GUI_UNBOX) == 19){
		unInput = true;
		pwInput = false;
		unBox->setText(L"");
	}


	return 0;
}

int killLoginScreen(){
	//animation for releasing login screen
	s32 alphaValue = 255;
	timer animTimer;
	unsigned int animTime = 100;
	animTimer.start();
	while (animTimer.getTime() <= animTime){
		unBox->move(vector2d<s32>(0, 1));
		pwBox->move(vector2d<s32>(0, 1));

		setSkinTransparency(alphaValue, thisClient->gui->getSkin());

		int tim = animTimer.getTime();
		float mod = (float)animTimer.getTime() / (float)animTime;

		alphaValue = 255 - mod * 255;

		thisClient->updateGraphics();
	}
	unBox->setVisible(false);
	pwBox->setVisible(false);
	title->setVisible(false);
	animTimer.stop();
	setSkinTransparency(255, thisClient->gui->getSkin());
	return 0;
}