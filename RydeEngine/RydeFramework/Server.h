
class Server{
		public:
			RakPeerInterface* peer;
			list<User*> userList;
			RakString serverDir;

			unsigned int port;
			unsigned int maxClients;

			Packet* packet;

			Server(unsigned int tmpPort = 60000, unsigned tmpMaxClients = 15){
				peer = RakPeerInterface::GetInstance();
				port = tmpPort;
				maxClients = tmpMaxClients;

				SocketDescriptor testSD(port, 0);
				peer->Startup(maxClients, &testSD, 1);
				peer->SetMaximumIncomingConnections(maxClients);

			}

			~Server(){
				RakPeerInterface::DestroyInstance(peer);
			}

			//This function is used to get the next packet in cue
			int nextPacket(){
				if (packet){
					peer->DeallocatePacket(packet);
					packet = peer->Receive();
				}
				else {
					packet = peer->Receive();
				}
				return 0;
			}

			MessageID getPacketHeader(){
				if (packet == NULL) return 255;
				if (packet->data[0] < 1) return 255;
				return packet->data[0];
			}

			int newSession(){
				User* tmpUser = new User;
				tmpUser->un = "[NULL]";
				tmpUser->sa = packet->systemAddress;
				NetMessage tmpMsg;
				stringc tmpStr;

				BitStream* bsIn = new BitStream(packet->data, packet->length, false);
				bsIn->IgnoreBytes(sizeof(MessageID));
				bsIn->Read(tmpUser->guid);
				userList.push_front(tmpUser);


				return 0;
			}

			int sendCommand(RydeMessage tmpCmd, RakString tmpData){
				BitStream bsOut;
				bsOut.Write((MessageID)tmpCmd);
				bsOut.Write(tmpData);
				peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false);
				return 0;
			}

			

			int readCommand(Packet* tmpPacket, const char* tmpStr){
				NetMessage tmpMsg;
				tmpMsg.nextMsg(tmpPacket);
				tmpMsg.read((const char*) tmpStr);
				return 0;
			}

			int readCommand(Packet* tmpPacket, RakString* tmpStr){

				BitStream* bsIn = new BitStream(tmpPacket->data, tmpPacket->length, false);
				bsIn->IgnoreBytes(sizeof(MessageID));
				bsIn->Read(tmpStr);



				//NetMessage tmpMsg;
				//tmpMsg.nextMsg(tmpPacket);
				//tmpMsg.read(tmpStr);
				return 0;
			}

			int sendBroadcast(RydeMessage tmpCmd, RakString tmpData, Packet* tmpPacket){
				NetMessage tmpMsg;
				tmpMsg.add((MessageID)tmpCmd);
				tmpMsg.add((RakString)tmpData);
				tmpMsg.sendAll(peer, tmpPacket);
				return 0;
			}

			User* getUser(SystemAddress tmpSA){
				
				for (User* tmpUser : userList){
					if (tmpUser->sa == tmpSA){
						return tmpUser;
					}
				}
				User* rtUser = new User;
				userList.push_front(rtUser);
				return rtUser;
			}

			User* getUser(User* tmpUser){
				return tmpUser;
			}

			

			bool login(SystemAddress tmpSA, RakString tmpUN, RakString tmpPW){

				std::string rUN;
				std::string rPW;

				RakString fileName = serverDir + "\\accounts.dat";
				
				ifstream accFile(fileName.C_String());
				
				while (accFile >> rUN){
					accFile >> rPW;


					RakString cUN = rUN.c_str();
					RakString cPW = rPW.c_str();

					if (tmpUN == cUN){
						if (tmpPW == cPW){
							User* tmpUser = getUser(tmpSA);
							tmpUser->un = tmpUN;
							printf("User %s logged in with IP %s\n", tmpUser->un.C_String(), tmpUser->sa.ToString());
							//userList.push_front(tmpUser);

							BitStream bsOut;
							bsOut.Write((MessageID)RN_UPDATE_POSITION);
							bsOut.Write(tmpUser->un);
							bsOut.Write((int)0);
							bsOut.Write((int)0);
							bsOut.Write((int)0);
							peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_SYSTEM_ADDRESS, true);

							accFile.close();
							return true;
						}
						else{
							accFile.close();
							return false;
						}
					}
				}

				//Need to add a database system to allow for the storage of usernames and encrypted passwords
				//Below works
				//if (tmpUN == (RakString)"ryan" | tmpUN == (RakString)"jacob"){
				//	if (tmpPW == (RakString)"password1"){
				//		User* tmpUser = getUser(tmpSA);
				//		tmpUser->un = tmpUN;
				//		printf("User %s logged in with IP %s\n", tmpUser->un.C_String(), tmpUser->sa.ToString());
				//		//userList.push_front(tmpUser);

				//		return true;
				//	}
				return false;
				//}
				
			}

			NetMessage* returnMsg(Packet* tmpPacket){
				NetMessage* tmpMsg = new NetMessage;
				tmpMsg->nextMsg(tmpPacket);
				return tmpMsg;
			}

		private:
			
	};
