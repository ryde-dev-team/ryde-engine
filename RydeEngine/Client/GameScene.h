ICameraSceneNode* mainCam;
ISceneNode* testCube;

//Load function for the scene
int loadGameScene(){
	testCube = thisClient->scene->addCubeSceneNode();
	testCube->setMaterialFlag(EMF_LIGHTING, false);

	thisClient->scene->addCubeSceneNode()->setPosition(vector3df(30, 0, 30));
	thisClient->scene->addCubeSceneNode()->setPosition(vector3df(30, 0, -30));
	thisClient->scene->addCubeSceneNode()->setPosition(vector3df(-30, 0, 30));
	thisClient->scene->addCubeSceneNode()->setPosition(vector3df(-30, 0, -30));

	mainCam = thisClient->scene->addCameraSceneNode();
	mainCam->setPosition(vector3df(0, 30, -40));
	mainCam->setParent(testCube);
	return 0;
}

//Update function
int updateGameScene(){
	if (textInput == false){
		vector3df cubePos = testCube->getPosition();

		if (thisClient->events.isKeyDown(KEY_KEY_W)){
			cubePos.Z += 1;
			thisClient->sendPos(cubePos);
		}
		if (thisClient->events.isKeyDown(KEY_KEY_S)){
			cubePos.Z -= 1;
			thisClient->sendPos(cubePos);
		}
		if (thisClient->events.isKeyDown(KEY_KEY_A)){
			cubePos.X -= 1;
			thisClient->sendPos(cubePos);
		}
		if (thisClient->events.isKeyDown(KEY_KEY_D)){
			cubePos.X += 1;
			thisClient->sendPos(cubePos);
		}

		testCube->setPosition(cubePos);
	}

	mainCam->setTarget(testCube->getPosition());
	
	for (User* tmpUser : thisClient->userList){
		if (tmpUser->node){
			vector3df currentPos = tmpUser->node->getAbsolutePosition();
			if (tmpUser->pos != currentPos){
				tmpUser->node->setPosition(tmpUser->pos);
			}
		}
		else {
			tmpUser->node = thisClient->scene->addCubeSceneNode();
			tmpUser->node->setPosition(tmpUser->pos);
		}
	}
	return 0;
}

//Free everything here
int killGameScene(){

	return 0;
}