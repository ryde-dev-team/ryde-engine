/*
In your project you will need to do the following:
1.Under references, reference the RydeFramework project
2.Under C++/C > General > Additional Include Directories paste C:\Users\Ryan\Desktop\RydeEngine\RydeEngine\RakNet-master\Source;C:\Users\Ryan\Desktop\RydeEngine\RydeEngine\irrlicht-1.8.1\include;C:\Users\Ryan\Desktop\RydeEngine\RydeEngine\RydeFramework;%(AdditionalIncludeDirectories)
3.Under C++/C > Code Generation > Runtime make sure it is set to /MT
4.Under C++/C > Preprocessor > Preprocessor Def delete all and add _CRT_NONSTDC_NO_DEPRECATE;_CRT_SECURE_NO_DEPRECATE;_WINSOCK_DEPRECATED_NO_WARNINGS;%(PreprocessorDefinitions)
*/

#include <string>
#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <ctime>
#include <time.h>
#include <fstream>
#include "Gets.h"
using namespace std;

#include "RydeGraphics.h"


#include "RydeNetwork.h"
	#include "User.h"
	#include "NetMessage.h"
	#include "Server.h"
	#include "Client.h"


class timer {
public:
	timer();
	void           start();
	void           stop();
	void           reset();
	bool           isRunning();
	unsigned long  getTime();
	bool           isOver(unsigned long seconds);
private:
	bool           resetted;
	bool           running;
	unsigned long  beg;
	unsigned long  end;
};

timer::timer() {
	resetted = true;
	running = false;
	beg = 0;
	end = 0;
}


void timer::start() {
	if (!running) {
		if (resetted)
			beg = (unsigned long)clock();
		else
			beg -= end - (unsigned long)clock();
		running = true;
		resetted = false;
	}
}


void timer::stop() {
	if (running) {
		end = (unsigned long)clock();
		running = false;
	}
}


void timer::reset() {
	bool wereRunning = running;
	if (wereRunning)
		stop();
	resetted = true;
	beg = 0;
	end = 0;
	if (wereRunning)
		start();
}


bool timer::isRunning() {
	return running;
}


unsigned long timer::getTime() {
	if (running)
		return ((unsigned long)clock() - beg);
	else
		return end - beg;
}


bool timer::isOver(unsigned long seconds) {
	return seconds >= getTime();
}

std::string ExePath() {
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}